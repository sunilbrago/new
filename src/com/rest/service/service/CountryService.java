package com.rest.service.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.rest.service.bean.Country;

public class CountryService {
	
	static HashMap<Integer, Country> countryIdMap=getCountryIdMap();
	
	
	public CountryService()
	{
		super();
		
		if(countryIdMap==null)
		{
			countryIdMap=new HashMap<Integer, Country>();
			
			Country i=new Country(1, "India", 100000);
			Country p=new Country(2, "Pakistan", 200000);
			Country c=new Country(3, "China", 300000);
			Country n=new Country(4, "Nepal", 400000);
			Country a=new Country(5, "Austrelia", 500000);
		
			countryIdMap.put(1, i);
			countryIdMap.put(2, p);
			countryIdMap.put(3, c);
			countryIdMap.put(4, n);
			countryIdMap.put(5, a);
		}
		
	}

	
	public List<Country> getAllCountries()
	{
		List<Country> countries =new ArrayList<Country>(countryIdMap.values());
		return countries;
	}
	
	public Country getCountry(int id)
	{
		Country country=countryIdMap.get(id);
		return country;
	}
	
	public Country addCountry(Country country)
	{
		country.setId(getMaxId() + 1);
		countryIdMap.put(country.getId(), country);
		return country;
		
	}
	
	public Country updateCountry(Country country)
	{
		if(country.getId()<0)
			return null;
		countryIdMap.put(country.getId(), country);
		
		return country;
	}
	
	public void deleteCountry(int id)
	{
		countryIdMap.remove(id);
	}
	
private static HashMap<Integer, Country> getCountryIdMap() {
		
		return countryIdMap;
	}
	
	
	public static int getMaxId() {
		int max=0;
		for(int id:countryIdMap.keySet())
		{
			if(max<=id)
				max=id;
		}
		return max;
	}


	
	

}
